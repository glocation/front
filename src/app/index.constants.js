/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('glocation')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('API', 'http://localhost:62795/api/');
})();
