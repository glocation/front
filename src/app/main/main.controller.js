(function() {
  'use strict';

  angular
    .module('glocation')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController( $timeout, toastr, rolesService, globersService, $log) {
    var vm = this;

    vm.awesomeThings = [];
    vm.classAnimation = '';
    vm.creationDate = 1461199270096;
    vm.showToastr = showToastr;
    vm.Projects =[];
    vm.Globers =[];
    vm.getGlobersByProjectId = getGlobersByProjectId;
    vm.lala = receiveValue;
    vm.dummyData = null;
    vm.dummyData =[];
    function receiveValue(value) {
      vm.dummyData[value]= {value: Math.random()}
    }

    rolesService.getRoles().then(function(response){
      vm.Projects = response.datos;
    })


    function getGlobersByProjectId(id){
      var request ={
        projectId: id
      };
      globersService.getGlobersByProject(request).then(function(response){
        vm.Globers = response.datos;
      })
    }



    function showToastr(message) {
      toastr.info(message);
      vm.classAnimation = '';
    }

  }
})();
