(function() {
  'use strict';

  angular
    .module('glocation')
    .factory('rolesService', rolesService);

  /** @ngInject */
  function rolesService($log, $http, API, $q) {

    var getRoles = function() {
      return $q(function(resolve) {
        $http.get(API + 'projects/').then(function(result) {
          resolve(result.data);
        });
      });
    };

    return{
      getRoles: getRoles
    };
  }
})();
