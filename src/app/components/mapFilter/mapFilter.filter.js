(function() {
  'use strict';

  angular
    .module('glocation')
    .filter('map_colour', filter);

  /** @ngInject */
  function filter() {
    return function (input) {
        var b = 255 - Math.floor(input * 255);
        var g = Math.floor(input * 255);
        return "rgba(130," + g + "," + b + ",1)";
    }
  }

})();
