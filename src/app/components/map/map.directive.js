(function() {
  'use strict';

  angular
    .module('glocation')
    .directive('map', region);

  /** @ngInject */
  function region($compile) {
    var directive = {
      restrict: 'EA',
      templateUrl: '/assets/images/mexicoHigh.svg',
      link: function (scope, element) {
            var regions = element[0].querySelectorAll('.land');
            angular.forEach(regions, function (path) {
                var regionElement = angular.element(path);
                regionElement.attr("region", "");
                regionElement.attr("dummy-data", "main.dummyData");
                regionElement.attr("hover-region", "main.hoverRegion");
                regionElement.attr("lala", "main.lala");
                $compile(regionElement)(scope);
            })
        }
    };

    return directive;
  }
})();
