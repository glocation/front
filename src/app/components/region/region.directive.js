(function() {
  'use strict';

  angular
    .module('glocation')
    .directive('region', region);

  /** @ngInject */
  function region($compile) {
    var directive = {
      restrict: 'A',
      scope: {
        dummyData: "=",
        lala: '='
      },
      link: function (scope, element) {
            scope.elementId = element.attr("id");
            scope.regionClick = function () {
                scope.lala(scope.elementId)
            };
            scope.regionMouseOver = function () {
                scope.hoverRegion = scope.elementId;
                element[0].parentNode.appendChild(element[0]);
            };
            element.attr("ng-click", "regionClick()");
            element.attr("ng-attr-fill", "{{dummyData[elementId].value | map_colour}}");
            // element.attr("ng-mouseover", "regionMouseOver()"); //<--- Add this
            element.attr("ng-class", "{active:hoverRegion==elementId}"); //<--- Add this
            element.removeAttr("region");
            $compile(element)(scope);
        }
    };

    return directive;
  }
})();
