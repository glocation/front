(function() {
  'use strict';

  angular
    .module('glocation')
    .factory('globersService', globers);

  /** @ngInject */
  function globers($log, $http, API, $q) {

    var getGlobersByProject = function(info) {
      return $q(function(resolve) {
        $http.post(API + 'globers/getGlober/project/', info).then(function(result) {
          resolve(result.data);
        });
      });
    };

    return{
      getGlobersByProject: getGlobersByProject
    };
  }
})();
