(function() {
  'use strict';

  angular
    .module('glocation', ['ngAnimate', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ui.router', 'ui.bootstrap', 'toastr','ui.select']);

})();
